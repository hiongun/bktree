# bktree

파이선 파일 한개입니다.

A pure-python BKTree for faster search for simalar strings among a set of dictionary words
This can be used for two kinds of applications: (1) suggesting correct words against spelling errors, (2) finding most similar words

딕셔너리(단어집합)에서 특정 단어와 문자열 유사도가 높은 단어를 찾아줍니다.
스펠링 오류 처리나, 유사 단어 찾기에 사용될 수 있습니다.


## how to use

    import bktree
    
    bk = bktree.BKTree()
    
    # You need first to add all the words into the BKTree, so that distance computation is done ahead of time.
    # 일단 단어를 트리구조에 모두 밀어 넣어야 합니다. 밀어 넣는 동안, 트리내에서는 효율적인 거리 계산을 위한 구조화가 일어납니다.

    bk.addword('abc')
    bk.addword('def')
    bk.addword('xzdff')
    bk.addword('czde')
    bk.addword('dx')
    bk.addword('drfx')
    bk.addword('ddg')
    bk.addword('dxg')

    # Later, you can ask the tree, for certain number of nearest similar words
    # 그중에 문자열 유사도가 가까운 n개의 단어를 찾아 냅니다.
    words = bk.nearest('dde', 3)
    for word in words:
        print("%s" % (word))

