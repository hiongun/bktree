''' BKTree Implementation'''


from collections import MutableMapping

__all__ = ['Node', 'BKTree', 'hamming_distance']

def hamming_distance(a, b):
    return sum([1 for a,b in zip(a,b) if a!=b])
    
def jacquard_distance(a, b):
#{
    union = {}
    intersection = {}
    for c in a:
        if not c in union:
            union[c] = 1
    for c in b:
        if not c in union:
            union[c] = 1
        else:
            intersection[c] = 1

    distance = int(float(len(intersection))/float(len(union))*100.0)
    return distance
#}

def text_distance(a,b):
#{
    dist = len(a) - len(b)
    if dist < 0: dist = -dist

    union = {}
    intersection = {}
    for c in a:
        if not c in union:
            union[c] = 1
    for c in b:
        if not c in union:
            union[c] = 1
        else:
            intersection[c] = 1
    for k in union:
        if k in intersection:
            pass
        else:
            dist += 1

    return dist
#}


class Node(MutableMapping):
#{
    def __init__(self, key, value=None):
        self.key = key
        self.value = value
        self.mapping = {}

    def __len__(self):
        return len(self.mapping)

    def __iter__(self):
        return iter(self.mapping)

    def __setitem__(self, key, value):
        self.mapping[key] = value

    def __getitem__(self, key):
        return self.mapping[key]

    def __delitem__(self, key):
        del self.mapping[key]
#}


class BKTree:
#{
    def __init__(self, func=text_distance):
    #{
        self.root = None
        self._distance = func
    #}

    def insert(self, key, value):
    #{
        if self.root is None:
            self.root = Node(key, value)
            return

        current = self.root
        while True:
            distance = self._distance(current.key, key)
            if distance in current:
                current = current[distance]
            else:
                current[distance] = Node(key, value)
                break
    #}

    def search(self, key, radius):
    #{
        remaining, results = [self.root], []
        while remaining:
            node = remaining.pop()
            distance = self._distance(node.key, key)
            if distance <= radius:
                results.append(node)
            remaining.extend([
                child for child_distance, child in node.items()
                if distance-radius <= child_distance <= distance+radius
            ])
        return results
    #}

    def addword(self, word):
    #{
        self.insert(word, word)
    #}

    def nearest(self, word, n):
    #{
        words = {}

        for i in range(0, 300):
            r1 = self.search(word, i)

            for r in r1:
                if r.key not in words:
                    words[r.key] = 1

                if len(words) >= n:
                    break

        return words
    #}
#}

if __name__ == '__main__':
#{
    bk = BKTree()

    bk.addword('abc')
    bk.addword('def')
    bk.addword('xzdff')
    bk.addword('czde')
    bk.addword('dx')
    bk.addword('drfx')
    bk.addword('ddg')
    bk.addword('dxg')

    words = bk.nearest('dde', 3)
    for word in words:
        print("%s" % (word))

#}

